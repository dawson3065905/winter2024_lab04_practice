public class Student{
	//1
	private String name;
	//2
	private int pronounsIndex;
	//3
	private String favActivity;
	//4
	private double semester;
	//5
	private int programIndex;
	//6
	public int amountLearnt;
	
	private String[] pronouns = {"he/him", "she/her", "they/them"};
	
	private String[] programs = {"Computer Science", "Interactive Media Arts", "English"};
	
	//for part 4
	/*public Student(){
		this.name = name;
		//2
		this.pronounsIndex = pronounsIndex;
		//3
		this.favActivity = favActivity;
		//4
		this.semester = semester;
		//5
		this.programIndex = programIndex;
		//6
		this.amountLearnt = 0
	}*/
	
	public Student(String name, int pronounsIndex, String favActivity, double semester, int programIndex){
		
		//1
		this.name = name;
		//2
		this.pronounsIndex = pronounsIndex;
		//3
		this.favActivity = favActivity;
		//4
		this.semester = semester;
		//5
		this.programIndex = programIndex;
	}
	
	//accessor
	public String getName() {
       return this.name;
	}
	
	public String getPronouns() {
       return this.pronouns[pronounsIndex];
	}
	
	public String getFavActivity() {
       return this.favActivity;
	}
	
	public double getSemester() {
       return this.semester;
	}
	
	public String getProgram() {
       return this.programs[programIndex];
	}
	
	public int getAmountLearnt() {
       return this.amountLearnt;
	}

	
	
	public String studentIntro(){
		return "Hello!" + 
		"\r\n My name is: " + name +
		"\r\n I go by: " + pronouns[pronounsIndex] +
		"\r\n My favorite activity is: " + favActivity +
		"\r\n I am in: " + programs[programIndex];
	}
	
	public String yearOfStudent(){
		
		double yearSpent = Math.ceil(semester/2);
	
		return "I have spent " + semester + 
		" semesters here." + "\r\n So this is my " + yearSpent + " year.";
	}
	
	public void study(int inputAmount){
		amountLearnt +=inputAmount;
	}
}